package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public int ifPyramidBuild(int len){

        //Решим уравнение:
        // (n + 1)n / 2 = len_list
        // n*n + n - len_list*2 = 0

        int discriminant = 1 + 4 * 1 * len * 2;
        double n1 = (-1 + Math.sqrt(discriminant))/2;
        double n2 = (-1 - Math.sqrt(discriminant))/2;

        int number_rows = -1;
        if (n1 > 0 && n1%1 == 0){
            number_rows = (int)n1;
        }
        if (n2 > 0 && n2%1 == 0){
            number_rows = (int)n2;
        }
        return number_rows;
    }


    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int len = inputNumbers.size();
        int number_rows = ifPyramidBuild(len);

        if (number_rows == -1){
            throw new CannotBuildPyramidException();
        }

        try {
            inputNumbers.sort(null);
        }catch (NullPointerException e){
            throw new CannotBuildPyramidException();
        }


        int number_col = number_rows * 2 - 1;
        int[][] piramid = new int[number_rows][number_col];

        int k = number_rows - 1;
        int r = 1;
        int t = 0;
        for (int i = 0; i < number_rows; i++){
            int count = r;
            int j = k;
            while (count > 0 && t < len) {
                piramid[i][j] = inputNumbers.get(t);
                t++;
                count--;
                j = j + 2;
            }
            r++;
            k--;
        }

        // TODO : Implement your solution here
        return piramid;
    }


}
