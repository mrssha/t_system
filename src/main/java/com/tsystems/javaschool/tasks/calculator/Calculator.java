package com.tsystems.javaschool.tasks.calculator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {

        if (statement == null){
            return null;
        }

        Pattern bad_pattern = Pattern.compile("-{2,}");
        Matcher matcher = bad_pattern.matcher(statement);
        if (matcher.find()){
            return null;
        }

        try {
            String result = find_parentheses("\\([\\d*+-/]+\\)", statement);
            double double_result = Double.parseDouble(result);

            if (Double.isNaN(double_result) || Double.isInfinite(double_result)) {
                return null;
            }

            int int_result = (int)(double_result * 10000 + (double_result>0 ? 0.5 : -0.5));
            double_result = (double)int_result/10000;

            String string_result;

            if (double_result%1 == 0){
                string_result = String.valueOf(Math.round(double_result));
            }
            else {
                string_result = String.valueOf(double_result);
            }

            return string_result;
        }
        catch (NumberFormatException e){
            return null;
        }
    }


    public static String find_parentheses(String pattern_string, String statement){
        Pattern pattern = Pattern.compile(pattern_string);
        Matcher matcher = pattern.matcher(statement);
        boolean found = false;
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            String result = String.format("I found the text" +
                            " \"%s\" starting at " +
                            "index %d and ending at index %d.%n",
                    matcher.group(),
                    matcher.start(),
                    matcher.end());
            found = true;

            String expression = matcher.group().substring(1, matcher.group().length()-1);
            String firstChange = calculate("(-?\\d+(\\.\\d+)?[*/]-?\\d+(\\.\\d+)?)", expression);
            String secondChange = calculate("(-?\\d+(\\.\\d+)?[+-]-?\\d+(\\.\\d+)?)", firstChange);

            matcher.appendReplacement(sb, secondChange);

        }
        String new_statement = matcher.appendTail(sb).toString();

        if(!found){
            String firstChange = calculate("(-?\\d+(\\.\\d+)?[*/]-?\\d+(\\.\\d+)?)", new_statement);
            String secondChange = calculate("(-?\\d+(\\.\\d+)?[+-]-?\\d+(\\.\\d+)?)", firstChange);
            return secondChange;
        }

        return find_parentheses(pattern_string, new_statement);
    }


    public static String calculate(String pattern_string, String expression){

        Pattern pattern = Pattern.compile(pattern_string);
        Matcher matcher = pattern.matcher(expression);
        boolean found = false;
        StringBuffer sb = new StringBuffer();
        if (matcher.find()) {
            String result1 = String.format("I found the text" +
                            " \"%s\" starting at " +
                            "index %d and ending at index %d.%n",
                    matcher.group(),
                    matcher.start(),
                    matcher.end());

            Pattern pattern_digit = Pattern.compile("(-?\\d+(\\.\\d+)?)");


            Matcher match_digit = pattern_digit.matcher(matcher.group());
            match_digit.find();
            Double result = Double.parseDouble(match_digit.group());
            match_digit.find();

            if (matcher.group().contains("*")){
                result *= Double.parseDouble(match_digit.group());
            }
            else if (matcher.group().contains("/")) {
                result /= Double.parseDouble(match_digit.group());
            }
            else {
                result += Double.parseDouble(match_digit.group());
            }

            matcher.appendReplacement(sb, result.toString());

            found = true;
        }
        String new_expression = matcher.appendTail(sb).toString();

        if(!found){
            return new_expression;

        }
        return calculate(pattern_string, new_expression);
    }

}
